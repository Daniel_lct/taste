/**
 * Menu toggle
 */

$(document).ready(function(){
    $('.nav__burger').on('click', function(){
        $('.header__dropdown-menu').addClass('header__dropdown_active')
        $('.nav__close').addClass('nav__close_active')
        $('.nav__burger').css('display', 'none')
        $('.header__socials-icon').addClass('header__socials_active')
    })
})

$(document).ready(function(){
    $('.nav__close').on('click', function(){
        $('.header__dropdown-menu').removeClass('header__dropdown_active')
        $('.nav__close').removeClass('nav__close_active')
        $('.nav__burger').css('display', 'block')
        $('.header__socials-icon').removeClass('header__socials_active')
    })
})


/**
 * Modal window
 */


const openModalButtons = document.querySelectorAll('[data-modal-target]')
const closeModalButtons = document.querySelectorAll('[data-close-button]')
const overlay = document.getElementById('overlay');

openModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = document.querySelector(button.dataset.modalTarget)
        openModal(modal)
    })
})

closeModalButtons.forEach(button => {
    button.addEventListener('click', () => {
        const modal = button.closest('.modal'); //select parent element html
        closeModal(modal)
    })
})

overlay.addEventListener('click', () => {
    const modals = document.querySelectorAll('.modal.active')
    modals.forEach(modal => {
        closeModal(modal)
    })
})

function openModal(modal){
    // if(modal == null){
    //     return
    // }
    if(modal == null) return
    modal.classList.add('active')
    overlay.classList.add('active')
}

function closeModal(modal){
    // if(modal == null){
    //     return
    // }
    if(modal == null) return
    modal.classList.remove('active')
    overlay.classList.remove('active')
}

/**
 * Form validation
 */

const form = document.getElementById('form');
const username = document.getElementById('username');
const email = document.getElementById('email');
const tel = document.getElementById('tel');

form.addEventListener('submit', e => {
    e.preventDefault();

    checkInputs();
});

function checkInputs() {
    // trim to remove the whitespaces
    const usernameValue = username.value.trim();
    const emailValue = email.value.trim();
    const telValue = tel.value.trim();

    if(usernameValue === '') {
        setErrorFor(username, 'Username cannot be blank');
    } else {
        setSuccessFor(username);
    }

    if(emailValue === '') {
        setErrorFor(email, 'Incorrect E-mail or Passoword');
    } else if (!isEmail(emailValue)) {
        setErrorFor(email, 'Not a valid email');
    } else {
        setSuccessFor(email);
    }

    if(telValue === '') {
        setErrorFor(tel, 'Incorrect Telephone or Passoword');
    } else if (!isTel(telValue)) {
        setErrorFor(tel, 'Not a valid email');
    } else {
        setSuccessFor(tel);
    }

}

function setErrorFor(input, message) {
    const formControl = input.parentElement;
    const small = formControl.querySelector('small');
    formControl.className = 'form-control error';
    // small.innerText = message;
}

function setSuccessFor(input) {
    const formControl = input.parentElement;
    formControl.className = 'form-control success';
}

function isEmail(email) {
    return /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/.test(email);
}


function isTel(tel) {
    return /^[\+0-9]{11,15}$/.test(tel);
}

/**
 * Phone Mask
 */

$("#tel").mask("+7(999)999-99-99");


/**
 * About Slider settings
 */

// const swiper = new Swiper('.swiper-container', {
//     pagination: {
//         el: '.swiper-pagination',
//         clickable: true,
//     },
// });


/**
 * Compare images
 */
$(document).ready(function(){
    $('.ba-slider').each(function(){
        var cur = $(this);
        // Adjust the slider
        var width = cur.width()+'px';
        cur.find('.resize img').css('width', width);
        // Bind dragging events
        drags(cur.find('.handle'), cur.find('.resize'), cur);
    });
});

// Update sliders on resize.
// Because we all do this: i.imgur.com/YkbaV.gif
$(window).resize(function(){
    $('.ba-slider').each(function(){
        var cur = $(this);
        var width = cur.width()+'px';
        cur.find('.resize img').css('width', width);
    });
});

function drags(dragElement, resizeElement, container) {

    // Initialize the dragging event on mousedown.
    dragElement.on('mousedown touchstart', function(e) {

        dragElement.addClass('draggable');
        resizeElement.addClass('resizable');

        // Check if it's a mouse or touch event and pass along the correct value
        var startX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;

        // Get the initial position
        var dragWidth = dragElement.outerWidth(),
            posX = dragElement.offset().left + dragWidth - startX,
            containerOffset = container.offset().left,
            containerWidth = container.outerWidth();

        // Set limits
        minLeft = containerOffset + 10;
        maxLeft = containerOffset + containerWidth - dragWidth - 10;

        // Calculate the dragging distance on mousemove.
        dragElement.parents().on("mousemove touchmove", function(e) {

            // Check if it's a mouse or touch event and pass along the correct value
            var moveX = (e.pageX) ? e.pageX : e.originalEvent.touches[0].pageX;

            leftValue = moveX + posX - dragWidth;

            // Prevent going off limits
            if ( leftValue < minLeft) {
                leftValue = minLeft;
            } else if (leftValue > maxLeft) {
                leftValue = maxLeft;
            }

            // Translate the handle's left value to masked divs width.
            widthValue = (leftValue + dragWidth/2 - containerOffset)*100/containerWidth+'%';

            // Set the new values for the slider and the handle.
            // Bind mouseup events to stop dragging.
            $('.draggable').css('left', widthValue).on('mouseup touchend touchcancel', function () {
                $(this).removeClass('draggable');
                resizeElement.removeClass('resizable');
            });
            $('.resizable').css('width', widthValue);
        }).on('mouseup touchend touchcancel', function(){
            dragElement.removeClass('draggable');
            resizeElement.removeClass('resizable');
        });
        e.preventDefault();
    }).on('mouseup touchend touchcancel', function(e){
        dragElement.removeClass('draggable');
        resizeElement.removeClass('resizable');
    });
}


/**
 * Show marketing kit on second screen
 */

$(document).ready(function(){
    if($(window).width() > 1300){
        $('.header__dropdown-markit').css('display', 'none')

        let i = 20
        $(document).on('scroll', function(){
            if(window.scrollY >= 800) {
                $('.header__dropdown-markit').css('display', 'flex')
            } else {
                $('.header__dropdown-markit').css('display', 'none')
            }


        })
    }


    $(document).on('wheel', function(){


    })

    if(window.scrollY >= 2600 && window.scrollY < 4000){

        function parallax(event){
            this.querySelectorAll('.parallax-leaf').forEach(i => {
                let speed = i.getAttribute('data-speed')
                i.style.transform = `translateX(${event.clientX * speed / 50}px)`
            })
            console.log(event)
        }

        document.addEventListener('mousemove', parallax)


    }

})
